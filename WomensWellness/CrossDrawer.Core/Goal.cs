﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WomensWellness.Core
{
    /// <summary>
    /// Developed by:
    /// Name: Natalie Sketcher
    /// Number: n9398554
    /// </summary>
    public class Goal
    {
        public double GoalNumber { get; set; }
        public string GoalName { get; set; }

        public Goal() { }
        public Goal(double goalNumber, string goalName)
        {
            GoalNumber = goalNumber;
            GoalName = goalName;
        }
    }

}
