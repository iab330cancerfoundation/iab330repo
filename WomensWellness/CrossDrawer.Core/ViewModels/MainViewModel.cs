﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using WomensWellness.Core.ViewModels;

namespace WomensWellness.Core
{
    /// <summary>
    /// Developed by:
    /// Name: Jacob McCurdy
    /// Number: N9398678
    /// </summary>
	public class MainViewModel : MvxViewModel
	{
		readonly Type[] _menuItemTypes = {
			typeof(ButtonViewModel),
			typeof(RecordsViewModel),
            typeof(GoalsViewModel),
            typeof(ProfileViewModel),
        };

		public IEnumerable<string> MenuItems { get; private set; } = new [] { "Home", "Records", "Goals", "Profile" };

		public void ShowDefaultMenuItem()
		{
			NavigateTo (0);
		}

		public void NavigateTo (int position)
		{
			ShowViewModel (_menuItemTypes [position]);
		}
	}

	public class MenuItem : Tuple<string, Type>
	{
		public MenuItem (string displayName, Type viewModelType)
			: base (displayName, viewModelType)
		{}

		public string DisplayName
		{
			get { return Item1; }
		}

		public Type ViewModelType
		{
			get { return Item2; }
		}
	}
}