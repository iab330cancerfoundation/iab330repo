using MvvmCross.Core.ViewModels;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace WomensWellness.Core.ViewModels
{
    /// <summary>
    /// Developed by:
    /// Name: Natalie Sketcher
    /// Number: n9398554
    /// </summary>
    public class GoalsViewModel
        : MvxViewModel
    {

        private string goalName;
        public string GoalName
        {
            get { return goalName; }
            set
            {
                if (value != null)
                {
                    SetProperty(ref goalName, value);
                }
            }
        }
        private string goalNumber;
        public string GoalNumber
        {
            get { return goalNumber; }
            set
            {
                if (value != null)
                {
                    SetProperty(ref goalNumber, value);
                }
            }
        }

        public GoalsViewModel()
        {

        

        }
     


        private ObservableCollection<Goal> goalList = new ObservableCollection<Goal>() {
            new Goal(10000, "Drink 5 glasses of water"),
            new Goal(5, "Drink 5 glasses of water"),
            new Goal(8,"Sleep for 8 hours"),
            new Goal(60, "Study for an hour"),
            new Goal(2,"Eat 2 fruit"),
            new Goal(5, "Eat 5 veggies"),
            new Goal(60, "Read for an hour"),
            new Goal(1, "Talk with a friend"),
            new Goal(7, "Meet your step goal for one week"),
            new Goal(7, "Get 8 hrs sleep for a week")
           

        };
        public ObservableCollection<Goal> GoalList
        {
            get { return goalList; }
            set { SetProperty(ref goalList, value); }


        }
    }
}
