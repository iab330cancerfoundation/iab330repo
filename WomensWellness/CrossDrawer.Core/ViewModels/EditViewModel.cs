﻿using MvvmCross.Core.ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace WomensWellness.Core.ViewModels
{
    /// <summary>
    /// Developed by:
    /// Name: Juan Dela Cruz
    /// Number: n9472151
    /// </summary>
    public class EditViewModel : MvxViewModel
    {

        public ICommand SaveDetailsCommand { get; private set; }
        public EditViewModel()
        {
            SaveDetailsCommand = new MvxCommand<ProfileViewModel>(select => ShowViewModel<ProfileViewModel>(select));
        }
        

        private string userName;
        public string UserName{
            get {
                return userName;
            }
            set {
                if (value != null) {
                    SetProperty(ref userName, value);
                }
            }
        }

        private string userBio;
        public string UserBio {
            get {
                return userBio;
            }
            set {
                if (value != null) {
                    SetProperty(ref userBio, value);
                }
            }
        }
    }
}
