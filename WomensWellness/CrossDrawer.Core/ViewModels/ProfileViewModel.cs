﻿using MvvmCross.Core.ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace WomensWellness.Core.ViewModels
{
    /// <summary>
    /// Developed by:
    /// Name: Juan Dela Cruz
    /// Number: n9472151
    /// </summary>
    public class ProfileViewModel : MvxViewModel
    {
      
        public ICommand EditDetailsCommand { get; private set; }
        public ProfileViewModel() {
            EditDetailsCommand = new MvxCommand<EditViewModel>(select => ShowViewModel<EditViewModel>(select));
        }
    }
}
