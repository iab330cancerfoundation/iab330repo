﻿using MvvmCross.Core.ViewModels;
using WomensWellness.Core.ViewModels;

namespace WomensWellness.Core
{
	public class App : MvxApplication
	{
		public override void Initialize()
		{
			base.Initialize();

			InitializeNavigation();
		}

		void InitializeNavigation()
		{
			RegisterAppStart<MainViewModel>();
		}
	}
}
