using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using MvvmCross.Droid.Views;
using System;

namespace WomensWellness.Droid.Views
{
    [Activity(Label = "Edit")]
    /// <summary>
    /// Developed by:
    /// Name: Juan Dela Cruz
    /// Number: N9472151
    /// </summary>
    public class EditView : MvxActivity
    {
        public static readonly int PickImageId = 1000;
        private ImageView _imageView;
        
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.EditView);

            _imageView = FindViewById<ImageView>(Resource.Id.userPic);
            Button button = FindViewById<Button>(Resource.Id.galleryButton);
            button.Click += ButtonOnClick;
        }
        // source of tutorial ----- developer.xamarin.com/recipes/android/other_ux/pick_image/
        private void ButtonOnClick(object sender, EventArgs eventArgs)
        {
            Intent = new Intent();
            Intent.SetType("image/*");
            Intent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(Intent, "Select Picture"), PickImageId);
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if ((requestCode == PickImageId) && (resultCode == Result.Ok) && (data != null))
            {
                Android.Net.Uri uri = data.Data;
                _imageView.SetImageURI(uri);
            }
        }

    }
        
}
