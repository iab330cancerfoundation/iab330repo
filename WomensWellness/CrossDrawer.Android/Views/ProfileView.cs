using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;

namespace WomensWellness.Droid.Views
{
    [Activity(Label = "Profile")]
    /// <summary>
    /// Developed by:
    /// Name: Juan Dela Cruz
    /// Number: N9472151
    /// </summary>
    public class ProfileView : MvxActivity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.ProfileView);
        }
    }
}
