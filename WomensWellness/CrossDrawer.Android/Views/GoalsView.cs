using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;

namespace WomensWellness.Droid.Views
{
    [Activity(Label = "View for GoalsViewModel")]
    /// <summary>
    /// Developed by:
    /// Name: Natalie Sketcher
    /// Number: n9398554
    /// </summary>
    public class GoalsView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.GoalsView);
        }
    }
}
