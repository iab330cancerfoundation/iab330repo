using Android.OS;
using Android.Runtime;
using Android.Views;
using MvvmCross.Droid.Support.V7.Fragging.Attributes;
using MvvmCross.Droid.Support.V7.Fragging.Fragments;
using WomensWellness.Core;
using WomensWellness.Core.ViewModels;

namespace WomensWellness.Droid
{
    /// <summary>
    /// Developed by:
    /// Name: Jacob McCurdy
    /// Number: N9398678
    /// </summary>
    [MvxFragmentAttribute(typeof(MainViewModel), Resource.Id.frameLayout)]
    [Register("womenswellness.droid.RecordsView")]
    public class RecordsView : MvxFragment<RecordsViewModel>
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.RecordsView, container, false);
        }
    }
}
