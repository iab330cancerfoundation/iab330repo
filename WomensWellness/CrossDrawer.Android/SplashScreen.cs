﻿using Android.App;
using Android.Content.PM;
using MvvmCross.Droid.Views;

namespace WomensWellness.Droid
{
	[Activity(
        Label = "WomensWellness",
        MainLauncher = true,
		NoHistory = true,
		ScreenOrientation = ScreenOrientation.Portrait)]
        
	public class SplashScreen : MvxSplashScreenActivity
	{
		public SplashScreen()
			: base(Resource.Layout.Splash)
		{
		}
	}
}

